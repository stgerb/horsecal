import 'package:flutter/material.dart';

class PasswordEntry extends StatefulWidget {
  final String hint;

  PasswordEntry({
    @required this.hint,
  });

  @override
  _PasswordEntryState createState() => _PasswordEntryState();
}

class _PasswordEntryState extends State<PasswordEntry> {
  bool obscurePassword;

  @override
  void initState() {
    super.initState();
    obscurePassword = true;
  }

  void setPasswordObscurity() {
    if (obscurePassword) {
      obscurePassword = false;
    } else {
      obscurePassword = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: obscurePassword,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(0),
        ),
        hintText: widget.hint,
        suffixIcon: IconButton(
          icon: Icon(obscurePassword ? Icons.visibility_off : Icons.visibility),
          onPressed: () {
            setState(() {
              setPasswordObscurity();
            });
          },
        ),
      ),
    );
  }
}