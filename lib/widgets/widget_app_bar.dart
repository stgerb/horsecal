import 'package:flutter/material.dart';
import 'package:horsecal/screens/widget_requests.dart';
import 'package:horsecal/screens/widget_settings.dart';

class AppBarWidget extends StatefulWidget with PreferredSizeWidget {
  final String title;
  final bool leading;
  final bool notification;
  final bool setting;

  AppBarWidget({
    @required this.title,
    @required this.leading,
    @required this.notification,
    @required this.setting,
  });

  @override
  _AppBarWidgetState createState() => _AppBarWidgetState();

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

class _AppBarWidgetState extends State<AppBarWidget> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: widget.preferredSize,
      child: AppBar(
        automaticallyImplyLeading: widget.leading,
        title: Text(widget.title),
        actions: <Widget>[
          Visibility(
            visible: widget.notification,
            child: IconButton(
              icon: Icon(Icons.notifications),
              onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => RequestsWidget())),
            ),
          ),
          Visibility(
            visible: widget.setting,
            child: IconButton(
              icon: Icon(Icons.settings), onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsWidget())),
            ),
          ),
        ],
      ),
    );
  }
}