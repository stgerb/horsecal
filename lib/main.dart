import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:horsecal/screens/widget_login.dart';

void main() => runApp(HorseCal());

class HorseCal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HorseCal',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
        accentColor: Colors.green,
        buttonColor: Colors.green,
        buttonTheme: ButtonThemeData(
          textTheme: ButtonTextTheme.primary,
        ),
        fontFamily: 'Roboto-Regular',
        textTheme: TextTheme(
          headline: TextStyle(
            fontSize: 36.0,
            fontWeight: FontWeight.bold,
            color: Colors.grey[600],
          ),
          body1: TextStyle(
            fontSize: 14.0,
            color: Colors.grey[600],
          ),
          caption: TextStyle(
            fontSize: 12.0,
            color: Colors.grey[600],
          ),
          title: TextStyle(
            fontSize: 24.0,
            color: Colors.grey[600],
          ),
          subhead: TextStyle(
            color: Colors.grey[600],
          ),
        ),
      ),
      home: LoginWidget(),
    );
  }
}