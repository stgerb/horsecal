import 'package:flutter/material.dart';
import 'package:horsecal/models/horse.dart';
import 'package:horsecal/models/requests.dart';
import 'package:horsecal/models/user.dart';
import 'package:horsecal/widgets/widget_app_bar.dart';

class RequestsWidget extends StatefulWidget {

  @override
  _RequestsWidgetState createState() => _RequestsWidgetState();
}

class _RequestsWidgetState extends State<RequestsWidget> {
  List<Requests> _requests = [
    new Requests(
        user: new User(
            firstName: 'Hans',
            lastName: 'Peter',
            email: 'email@email.com',
            password: 'test1234'),
        horse: new Horse(
            name: 'Jolly Jumper',
            birthdate: '01.01.2011',
            owner: new User(
                firstName: 'Bettina',
                lastName: 'Morgenthaler',
                email: 'email@email.com',
                password: 'test1234'),
            url: 'horse1.png')),
    new Requests(
        user: new User(
            firstName: 'Johnny',
            lastName: 'Walker',
            email: 'email@email.com',
            password: 'test1234'),
        horse: new Horse(
            name: 'Horsi',
            birthdate: '01.01.2012',
            owner: new User(
                firstName: 'Bettina',
                lastName: 'Morgenthaler',
                email: 'email@email.com',
                password: 'test1234'),
            url: 'horse2.png')),
    new Requests(
        user: new User(
            firstName: 'Andrin',
            lastName: 'Hübscher',
            email: 'email@email.com',
            password: 'test1234'),
        horse: new Horse(
            name: 'Jolly Jumper',
            birthdate: '01.01.2011',
            owner: new User(
                firstName: 'Bettina',
                lastName: 'Morgenthaler',
                email: 'email@email.com',
                password: 'test1234'),
            url: 'horse1.png')),
    new Requests(
        user: new User(
            firstName: 'Andrin',
            lastName: 'Hübscher',
            email: 'email@email.com',
            password: 'test1234'),
        horse: new Horse(
            name: 'Jolly Jumper',
            birthdate: '01.01.2011',
            owner: new User(
                firstName: 'Bettina',
                lastName: 'Morgenthaler',
                email: 'email@email.com',
                password: 'test1234'),
            url: 'horse1.png')),
    new Requests(
        user: new User(
            firstName: 'Andrin',
            lastName: 'Hübscher',
            email: 'email@email.com',
            password: 'test1234'),
        horse: new Horse(
            name: 'Jolly Jumper',
            birthdate: '01.01.2011',
            owner: new User(
                firstName: 'Bettina',
                lastName: 'Morgenthaler',
                email: 'email@email.com',
                password: 'test1234'),
            url: 'horse1.png')),
    new Requests(
        user: new User(
            firstName: 'Andrin',
            lastName: 'Hübscher',
            email: 'email@email.com',
            password: 'test1234'),
        horse: new Horse(
            name: 'Jolly Jumper',
            birthdate: '01.01.2011',
            owner: new User(
                firstName: 'Bettina',
                lastName: 'Morgenthaler',
                email: 'email@email.com',
                password: 'test1234'),
            url: 'horse1.png')),
    new Requests(
        user: new User(
            firstName: 'Andrin',
            lastName: 'Hübscher',
            email: 'email@email.com',
            password: 'test1234'),
        horse: new Horse(
            name: 'Jolly Jumper',
            birthdate: '01.01.2011',
            owner: new User(
                firstName: 'Bettina',
                lastName: 'Morgenthaler',
                email: 'email@email.com',
                password: 'test1234'),
            url: 'horse1.png')),
    new Requests(
        user: new User(
            firstName: 'Andrin',
            lastName: 'Hübscher',
            email: 'email@email.com',
            password: 'test1234'),
        horse: new Horse(
            name: 'Jolly Jumper',
            birthdate: '01.01.2011',
            owner: new User(
                firstName: 'Bettina',
                lastName: 'Morgenthaler',
                email: 'email@email.com',
                password: 'test1234'),
            url: 'horse1.png')),
    new Requests(
        user: new User(
            firstName: 'Andrin',
            lastName: 'Hübscher',
            email: 'email@email.com',
            password: 'test1234'),
        horse: new Horse(
            name: 'Jolly Jumper',
            birthdate: '01.01.2011',
            owner: new User(
                firstName: 'Bettina',
                lastName: 'Morgenthaler',
                email: 'email@email.com',
                password: 'test1234'),
            url: 'horse1.png'))
  ];

  @override
  void initState() {
    super.initState();
  }

  Widget _listItemBuilder(BuildContext context, int index) {
    return Container(
      height: 85.0,
      child: Card(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  SizedBox(width: 16.0),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        '${_requests[index].user.firstName} ${_requests[index].user.lastName}'),
                      SizedBox(height: 4.0),
                      Text('Anfrage für ${_requests[index].horse.name}', style: Theme.of(context).textTheme.caption),
                    ],
                  ),
                ],
              ),
            ),
            IconButton(
              icon: Icon(Icons.check),
              color: Colors.grey[600],
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.close),
              color: Colors.grey[600],
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Anfragen',
        leading: true,
        notification: false,
        setting: true,
      ),
      body: ListView.builder(
        padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        shrinkWrap: true,
        itemCount: _requests.length,
        itemBuilder: _listItemBuilder
      ),
    );
  }
}