import 'package:flutter/material.dart';
import 'package:horsecal/screens/widget_calendar_members.dart';
import 'package:horsecal/widgets/widget_app_bar.dart';

class CalendarWidget extends StatefulWidget {
  final String heading;
  final bool icon;

  CalendarWidget({@required this.heading, @required this.icon});

  @override
  _CalendarWidgetState createState() => _CalendarWidgetState();
}

class _CalendarWidgetState extends State<CalendarWidget> {

  @override
  void initState() {
    super.initState();
  }

  Widget _buildHeadline(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Text(
            widget.heading,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.title,
          ),
        ),
        Visibility(
          visible: widget.icon,
          child: IconButton(
            icon: Icon(Icons.group),
            color: Colors.grey[600],
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => CalendarMembersWidget(heading: widget.heading))),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: 'HorseCal',
        leading: true,
        notification: true,
        setting: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        child: _buildHeadline(context),
      ),
    );
  }
}