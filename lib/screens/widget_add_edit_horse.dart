import 'package:flutter/material.dart';
import 'package:horsecal/widgets/widget_app_bar.dart';
import 'package:horsecal/widgets/widget_entry_name.dart';

class AddEditHorseWidget extends StatefulWidget {
  final String title;
  final String button;

  AddEditHorseWidget({@required this.title, @required this.button});

  @override
  _AddEditHorseWidgetState createState() => _AddEditHorseWidgetState();
}

class _AddEditHorseWidgetState extends State<AddEditHorseWidget> {

  @override
  void initState() {
    super.initState();
  }

  Widget _buildImage(BuildContext context) {
    return SafeArea(
      child: Image(
        image: AssetImage('assets/images/img_placeholder.png'),
        height: MediaQuery.of(context).size.width,
        width: MediaQuery.of(context).size.width,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _buildBirthdayTextField() {
    return TextField(
      keyboardType: TextInputType.datetime,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(0),
        ),
        hintText: 'Geburtsdatum',
        suffixIcon: IconButton(
          icon: Icon(Icons.calendar_today),
          onPressed: () {

          },
        ),
      ),
    );
  }

  Widget _buildCreateButton(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        RaisedButton(
          child: Text(widget.button),
          onPressed: () => Navigator.pop(context, false),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: widget.title,
        leading: true,
        notification: true,
        setting: true,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 24.0),
        child: Column(
          children: <Widget>[
            _buildImage(context),
            SizedBox(height: 16.0),
            NameEntry(name: 'Pferdname'),
            SizedBox(height: 8.0),
            _buildBirthdayTextField(),
            SizedBox(height: 8.0),
            _buildCreateButton(context),
          ],
        ),
      ),
    );
  }
}
