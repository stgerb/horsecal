import 'package:flutter/material.dart';
import 'package:horsecal/widgets/widget_app_bar.dart';
import 'package:horsecal/widgets/widget_entry_email.dart';
import 'package:horsecal/widgets/widget_entry_name.dart';
import 'package:horsecal/widgets/widget_entry_password.dart';

class SettingsWidget extends StatelessWidget {

  Widget _buildNameTextFields() {
    return SafeArea(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
            child: NameEntry(name: 'Vorname'),
          ),
          SizedBox(width: 8.0),
          Expanded(
            child: NameEntry(name: 'Nachname'),
          ),
        ],
      ),
    );
  }

  Widget _buildUpdateNameButton() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        RaisedButton(
          child: Text('Anzeigename ändern'),
          onPressed: () {

          },
        ),
      ],
    );
  }

  Widget _buildUpdateEmailButton() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        RaisedButton(
          child: Text('E-Mail-Adresse ändern'),
          onPressed: () {

          },
        ),
      ],
    );
  }

  Widget _buildPasswordTextFields() {
    return Column(
      children: <Widget>[
        PasswordEntry(hint: 'Neues Passwort'),
        SizedBox(height: 8.0),
        PasswordEntry(hint: 'Passwort bestätigen'),
      ],
    );
  }

  Widget _buildButtons(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        RaisedButton(
          child: Text('Passwort ändern'),
          onPressed: () {

          },
        ),
        RaisedButton(
          child: Text('Abmelden'),
          color: Colors.lightGreen[400],
          textColor: Colors.white,
          onPressed: () {

          },
        ),
        RaisedButton(
          child: Text(
            'Konto löschen',
          ),
          color: Colors.red[400],
          onPressed: () => showDialog(context: context, builder: (context) => _dialogBuilder(context)),
        ),
      ],
    );
  }

  Widget _dialogBuilder(BuildContext context) {
    return SimpleDialog(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(16.0),
          child: Text('Konto löschen?', style: Theme.of(context).textTheme.subhead),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton(
              child: Text('Abbrechen'),
              textColor: Colors.grey[600],
              onPressed: () => Navigator.pop(context, false),
            ),
            RaisedButton(
              child: Text('Löschen'),
              color: Colors.red[400],
              onPressed: () {},
            )
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: 'Einstellungen',
        leading: true,
        notification: true,
        setting: false,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
        child: Column(
          children: <Widget>[
            _buildNameTextFields(),
            SizedBox(height: 8.0),
            _buildUpdateNameButton(),
            SizedBox(height: 16.0),
            EmailEntry(),
            SizedBox(height: 8.0),
            _buildUpdateEmailButton(),
            SizedBox(height: 16.0),
            _buildPasswordTextFields(),
            SizedBox(height: 8.0),
            _buildButtons(context),
          ],
        ),
      ),
    );
  }
}