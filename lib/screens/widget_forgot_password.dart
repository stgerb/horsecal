import 'package:flutter/material.dart';
import 'package:horsecal/widgets/widget_entry_email.dart';

class ForgotPasswordWidget extends StatelessWidget {

  Widget _buildButton(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        RaisedButton(
          child: Text('Zurücksetzen'),
          onPressed: () => Navigator.pop(context, false),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Passwort zurücksetzen',
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
        child: Column(
          children: <Widget>[
            EmailEntry(),
            SizedBox(height: 8.0),
            _buildButton(context),
          ],
        ),
      ),
    );
  }
}